#include <SoftwareSerial.h>

SoftwareSerial mySerial(8, 9, false); // RX, TX

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(38400);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }


  Serial.println("Goodnight moon!");

  // set the data rate for the SoftwareSerial port
  mySerial.begin(38400);
}

void loop() { // run over and over
  if (Serial.available()) {
    int incomingByte = Serial.read();
    mySerial.write(incomingByte);
  }
  if (mySerial.available()) {
    int incomingByte = mySerial.read();
    Serial.write((char)incomingByte);
  }
}
