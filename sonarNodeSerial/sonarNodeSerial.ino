//#include <Ports.h>
//#include <PortsBMP085.h>
//#include <PortsLCD.h>
//#include <PortsSHT11.h>
#include <RF69.h>
#include <RF69_avr.h>
#include <RF69_compat.h>
#include <JeeLib.h>
#include <RF12.h>
#include <RF12sio.h>
#include "EmonLib.h"

#define RF69_COMPAT 1
#define SERIAL  1   // set to 1 to also report readings on the serial port

#define MEASURE_PERIOD_SECONDS  10 * 20 // how often to measure, in tenths of seconds
#define RETRY_PERIOD    10  // how soon to retry if ACK didn't come in
#define RETRY_LIMIT     5   // maximum number of times to retry
#define ACK_TIME        1000  // number of milliseconds to wait for an ack

#define DRIVE_PIN 4
#define READ_PIN 7

#define RADIO_SYNC_MODE 2
#define freq RF12_868MHZ

// The scheduler makes it easy to perform various tasks at various times:
enum { MEASURE, REPORT, TASK_END };

//very good vcc functuinality
EnergyMonitor monitor;

static word schedbuf[TASK_END];
Scheduler scheduler (schedbuf, TASK_END);

// Other variables used in various places in the code:

const byte nodeID = 20;                                                  // emonTx RFM12B node ID
const int networkGroup = 210;

struct {
  int distance; // distance in cm
  byte lobat;  // supply voltage dropped under 3.1V: 0..1
  int bat; // temperature: -500..+500 (tenths)
} payload;

// has to be defined because we're using the watchdog for low-power waiting
ISR(WDT_vect) {
  Sleepy::watchdogEvent();
}

void setup() {
#if SERIAL
  Serial.begin(9600);
  Serial.println("\n[Setup sonar] " + nodeID);
#endif

  pinMode(READ_PIN, INPUT);
  pinMode(DRIVE_PIN, OUTPUT);
  digitalWrite(DRIVE_PIN, LOW);

  rf12_initialize(nodeID, freq, networkGroup);
  rf12_sleep(RF12_SLEEP); // power down
  scheduler.timer(MEASURE, 0);
}

// wait a few milliseconds for proper ACK to me, return true if indeed received
static byte waitForAck() {
  MilliTimer ackTimer;
  while (!ackTimer.poll(ACK_TIME)) {
    int done = rf12_recvDone();
    if (done && (rf12_crc & 0xFFFE) == 0 &&
        // see http://talk.jeelabs.net/topic/811#post-4712
        rf12_hdr == (RF12_HDR_DST | RF12_HDR_CTL | nodeID)) {
#if SERIAL
      Serial.println("ACK Received");
      Serial.print("Node ID:"); Serial.println(rf12_hdr & RF12_HDR_MASK);
      Serial.println("received something");
      Serial.print("rf12_hdr="); Serial.println(rf12_hdr, HEX);
      Serial.print("rf12_crc="); Serial.println(~rf12_crc, HEX);
      Serial.print("RF12_HDR_CTL="); Serial.println(rf12_hdr & RF12_HDR_CTL);
      Serial.print("RF12_HDR_ACK="); Serial.println(rf12_hdr & RF12_HDR_ACK);
      Serial.print("rf12_len="); Serial.println(rf12_len, DEC);
#endif
      return 1;
    }
  }
#if SERIAL
  Serial.println("ACK not Received");
//  Serial.print("Node ID:"); Serial.println(rf12_hdr & RF12_HDR_MASK);
//  Serial.println("received something");
//  Serial.print("rf12_hdr="); Serial.println(rf12_hdr, HEX);
//  Serial.print("needed rf12_hdr="); Serial.println((RF12_HDR_DST | RF12_HDR_CTL | nodeID), HEX);
//  Serial.print("rf12_crc="); Serial.println(rf12_crc, HEX);
//  Serial.print("rf69_crc="); Serial.println(rf69_crc, HEX);
//  Serial.print("RF12_HDR_DST="); Serial.println(rf12_hdr & RF12_HDR_DST);
//  Serial.print("RF12_HDR_CTL="); Serial.println(rf12_hdr & RF12_HDR_CTL);
//  Serial.print("RF12_HDR_ACK="); Serial.println(rf12_hdr & RF12_HDR_ACK);
//  Serial.print("rf12_len="); Serial.println(rf12_len, DEC);
#endif
  return 0;
}

float measure() {
  digitalWrite(DRIVE_PIN, HIGH);
  delay(200);
  int measureVal = 0;
  String val = "R0";
  unsigned long first = 0;
  float elapsed = 0;
  byte up = 0;

  val = Serial.readString();
  val.substring(1, val.length());
  measureVal = val.toInt();
  digitalWrite(DRIVE_PIN, LOW);
  return measureVal;
  
//  while (true) {
//    //if(diffWStart > 25000 && !stop) {
//    //    digitalWrite(DRIVE_PIN, LOW);
//    //    stop = 1;
//    //}
//    measureVal = digitalRead(READ_PIN);
//    //    Serial.println(measureVal);
//    if (measureVal == 1 && !up) {
//      first = micros();
//      Serial.println(first);
//      up = 1;
//      Serial.println("-------UP---------");
//      digitalWrite(DRIVE_PIN, LOW);
//    }
//    if (measureVal == 0 && up) {
//      up = 0;
//      elapsed = (micros() - first) / (58);
//      Serial.print(elapsed);
//      Serial.println("cm Going down");
//      return elapsed;
//    }
//  }
}

static void serialFlush () {
#if ARDUINO >= 100
  Serial.flush();
#endif
  delay(2); // make sure tx buf is empty before going back to sleep
}

// send packet and wait for ack when there is a motion trigger
static void doSendData() {
#if SERIAL
  Serial.print("ROOM distance");
  Serial.print((int) payload.distance);
  Serial.println();
  serialFlush();
#endif

  for (byte i = 0; i < RETRY_LIMIT; ++i) {
    rf12_sleep(RF12_WAKEUP);
    rf12_sendNow(RF12_HDR_ACK, &payload, sizeof payload);
    rf12_sendWait(RADIO_SYNC_MODE);
    byte acked = waitForAck();
    rf12_sleep(RF12_SLEEP);

    if (acked) {
#if SERIAL
      Serial.print(" ack ");
      Serial.println((int) i);
      serialFlush();
#endif
      // reset scheduling to start a fresh measurement cycle
      scheduler.timer(MEASURE, MEASURE_PERIOD_SECONDS);
      return;
    }

    delay(RETRY_PERIOD * 100);
  }
  scheduler.timer(MEASURE, MEASURE_PERIOD_SECONDS);
#if SERIAL
  Serial.println(" no ack!");
  serialFlush();
#endif
}

static void doMeasure() {
  payload.distance = measure();
  payload.lobat = 0;
  payload.bat = monitor.readVcc(); 
}

void loop() {
  switch (scheduler.pollWaiting()) {
    case MEASURE:
      doMeasure();
      scheduler.timer(REPORT, 0);
      break;

    case REPORT:
      doSendData();
      break;
  }
}
