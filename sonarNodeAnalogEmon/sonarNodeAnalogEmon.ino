//Copyright 2013 Raino Kolk<raino.kolk@gmail.com>
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.

//Used code form RoomNode
// 2010-10-19 Jean-Claude Wippler<jc[at]wippler.nl> http://opensource.org/licenses/mit-license.php

// see http://jeelabs.org/2010/10/20/new-roomnode-code/
// and http://jeelabs.org/2010/10/21/reporting-motion/


//sonar datasheet http://www.maxbotix.com/documents/MB7066-MB7076_Datasheet.pdf

#include <JeeLib.h>
#include <avr/sleep.h>
#include <util/atomic.h>
#include "EmonLib.h"

#define SERIAL  1   // set to 1 to also report readings on the serial port
#define DEBUG   1   // set to 1 to display each loop() run and PIR trigger

#define RADIO_SYNC_MODE 2
#define freq RF12_868MHZ

#define MEASURE_PERIOD  100 // how often to measure, in tenths of seconds
#define MEASURE_COUNT  1 // how many times to count in cycle
#define RETRY_PERIOD    10  // how soon to retry if ACK didn't come in
#define RETRY_LIMIT     5   // maximum number of times to retry
#define ACK_TIME        10  // number of milliseconds to wait for an ack
#define REPORT_EVERY    1   // report every N measurement cycles
#define SMOOTH          3   // smoothing factor used for running averages

enum { MEASURE, REPORT, TASK_END };

static word schedbuf[TASK_END];
Scheduler scheduler (schedbuf, TASK_END);

const byte nodeID = 21;
const int networkGroup = 210;

static byte reportCount = 0;    // count up until next report, i.e. packet send
static byte myNodeID = nodeID;       // node ID used for this unit

const int SONAR_PIN = A2;
const int SONAR_POWER_PIN = 6;

const float threshold = 2.1 / 0.0032;
const float thresholdMax = 3.0 / 0.0032;

unsigned long values[6] = {0, 0, 0, 0, 0, 0};
byte counts[6] = {0, 0, 0, 0, 0, 0};


// This defines the structure of the packets which get sent out by wireless:

struct {
  unsigned int distanceMs0  :16;  // distance: 24..1061
  unsigned int distanceMs1  :16;  // distance: 24..1061
  unsigned int distanceMs2  :16;  // distance: 24..1061
  unsigned int distanceMs3  :16;  // distance: 24..1061
  unsigned int distanceMs4  :16;  // distance: 24..1061
  unsigned int distanceMs5  :16;  // distance: 24..1061
  
  byte lobat :8;  // supply voltage dropped under 3.1V: 0..1
  int bat    :16; // battery
} payload;

EnergyMonitor monitor;
ISR(WDT_vect) { Sleepy::watchdogEvent(); }

// utility code to perform simple smoothing as a running average
static int smoothedAverage(int prev, int next, byte firstTime =0) {
    if (firstTime)
        return next;
    return ((SMOOTH - 1) * prev + next + SMOOTH / 2) / SMOOTH;
}

// wait a few milliseconds for proper ACK to me, return true if indeed received
static byte waitForAck() {
    MilliTimer ackTimer;
    while (!ackTimer.poll(ACK_TIME)) {
        if (rf12_recvDone() && rf12_crc == 0 &&
                // see http://talk.jeelabs.net/topic/811#post-4712
                rf12_hdr == (RF12_HDR_DST | RF12_HDR_CTL | myNodeID))
            return 1;
        set_sleep_mode(SLEEP_MODE_IDLE);
        sleep_mode();
    }
    return 0;
}

void doMeasure(){
  byte measureCount = 0;
  
  for(int i = 0; i<6;i++) {
    values[i] = 0;
    counts[i] = 0;
  }
  
  while (measureCount < MEASURE_COUNT){
    digitalWrite(SONAR_POWER_PIN, HIGH);
    
    unsigned long start = micros();
    unsigned long first = start;
    unsigned long current = first;
    unsigned long diffWFirst = 0;
    unsigned long diffWStart = 0;
  
    int measureVal = 0;
    boolean currentLevel = 0;
  
    boolean stop = 0;
    boolean firstSignal = 1;
    byte up = 0;
    byte down = 0;
    
    while ((first <= current) && (diffWStart < 200000)) {
      if(diffWStart > 25000 && !stop) {
        digitalWrite(SONAR_POWER_PIN, LOW);
        stop = 1;
      }
      measureVal = analogRead(SONAR_PIN);
    
      if(measureVal > threshold) {
        if(!currentLevel) {
          if(firstSignal){
            up = 0;
            firstSignal = 0;
          } else {
            up = up + 1;
          }
        } 
        currentLevel = 1;
      } else {
        if(currentLevel) {
          if(up == 0){
            down = 0;
            first = micros();
            current = first;  
            diffWFirst = 0;
          } else {
            down = down + 1;
          }
          if(down < 6) {
            values[down] = values[down] + diffWFirst;
            counts[down] = counts[down] + 1;
          }
        }  
        currentLevel = 0;
      }
      current = micros();
      diffWFirst = current - first;
      diffWStart = current - start;
    }
    measureCount++;
    delay(100);
  }
}

// readout all the sensors and other values
static void triggerMeasure() {
  payload.lobat = rf12_lowbat();
  doMeasure();
    
  #if SERIAL
    for(int i = 0; i<6;i++) {
      Serial.print(i);
      Serial.print(":");
      if(counts[i] > 0){
        Serial.print((values[i] / counts[i])); 
        Serial.println(); 
      }
    }
    serialFlush();
  #endif
  
  payload.distanceMs0 = counts[0] > 0 ? values[0] / counts[0] : 0;
  payload.distanceMs1 = counts[1] > 0 ? values[1] / counts[1] : 0;
  payload.distanceMs2 = counts[2] > 0 ? values[2] / counts[2] : 0;
  payload.distanceMs3 = counts[3] > 0 ? values[3] / counts[3] : 0;
  payload.distanceMs4 = counts[4] > 0 ? values[4] / counts[4] : 0;
  payload.distanceMs5 = counts[5] > 0 ? values[5] / counts[5] : 0;
  payload.bat = monitor.readVcc();
}

static void serialFlush () {
    #if ARDUINO >= 100
        Serial.flush();
    #endif  
    delay(2); // make sure tx buf is empty before going back to sleep
}

void setup() {
  pinMode(SONAR_POWER_PIN, OUTPUT);
  digitalWrite(SONAR_POWER_PIN, LOW);
  
  pinMode(SONAR_PIN, INPUT);
  #if SERIAL || DEBUG
    Serial.begin(57600);
    Serial.print("\n[Setup sonar] ");
    Serial.print(nodeID);
    Serial.println();
    serialFlush();
  #endif
  rf12_initialize(nodeID, freq, networkGroup);  
  scheduler.timer(MEASURE, 0);    // start the measurement loop going
}

// periodic report, i.e. send out a packet and optionally report on serial port
static byte doReport() {
  rf12_sleep(RF12_WAKEUP);
  rf12_sendNow(RF12_HDR_ACK, &payload, sizeof payload);
  rf12_sendWait(RADIO_SYNC_MODE);
  byte acked = waitForAck();
  rf12_sleep(RF12_SLEEP);
    
  #if SERIAL
    Serial.print("Sonar ");
    Serial.print((int) payload.distanceMs0);
    Serial.print(", ");
    Serial.print((int) payload.distanceMs1);
    Serial.print(", ");
    Serial.print((int) payload.distanceMs2);
    Serial.print(", ");
    Serial.print((int) payload.distanceMs3);
    Serial.print(", ");
    Serial.print((int) payload.distanceMs4);
    Serial.print(", ");
    Serial.print((int) payload.distanceMs5);
    Serial.print(", ");
    Serial.print(" acked ");
    Serial.print(acked);
    Serial.println();
    serialFlush();
  #endif
  return acked;
}

static void triggerReport() {
  #if SERIAL
    Serial.print("Report sonar ");
    Serial.println();
    serialFlush();
  #endif

  for (byte i = 0; i < RETRY_LIMIT; ++i) {
    byte acked = doReport();
    if (acked) {
      #if SERIAL
        Serial.print(" ack received on retry ");
        Serial.println((int) i);
        serialFlush();
      #endif
      // reset scheduling to start a fresh measurement cycle
      scheduler.timer(MEASURE, MEASURE_PERIOD);
      return;
    }
    delay(RETRY_PERIOD * 100);
  }
  scheduler.timer(MEASURE, MEASURE_PERIOD);
  #if DEBUG
    Serial.println(" no ack!");
    serialFlush();
  #endif
}


void loop() {
  #if DEBUG
    Serial.print('.');
    serialFlush();
  #endif
  switch (scheduler.pollWaiting()) {
    case MEASURE:
      serialFlush();           
      triggerMeasure();
      delay(50);
      scheduler.timer(REPORT, 0);
      break;
            
    case REPORT:
      triggerReport();
      break;
  }
}
