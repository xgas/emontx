//#include <Ports.h>
//#include <PortsBMP085.h>
//#include <PortsLCD.h>
//#include <PortsSHT11.h>

#include <RFM69.h>         //get it here: https://www.github.com/lowpowerlab/rfm69
#include <RFM69_ATC.h>     //get it here: https://www.github.com/lowpowerlab/rfm69
#include <SPIFlash.h>      //get it here: https://www.github.com/lowpowerlab/spiflash
#include <SPI.h>           //included with Arduino IDE install (www.arduino.cc)

#include <LowPower.h>

//*********************************************************************************************
//************ IMPORTANT SETTINGS - YOU MUST CHANGE/CONFIGURE TO FIT YOUR HARDWARE ************
//*********************************************************************************************
//11 magamistuba
//12 lastetuba
//13 meriel
//14 elutuba
#define NODEID        12    //must be unique for each node on same network (range up to 254, 255 is used for broadcast)
#define NETWORKID     210  //the same on all nodes that talk to each other (range up to 255)
#define GATEWAYID     1
//Match frequency to the hardware version of the radio on your Moteino (uncomment one):
//#define FREQUENCY   RF69_433MHZ
#define FREQUENCY   RF69_868MHZ
//#define FREQUENCY     RF69_915MHZ
#define ENCRYPTKEY    "sampleEncryptKey" //exactly the same 16 characters/bytes on all nodes!
//#define IS_RFM69HW    //uncomment only for RFM69HW! Leave out if you have RFM69W!
//*********************************************************************************************
//Auto Transmission Control - dials down transmit power to save battery
//Usually you do not need to always transmit at max output power
//By reducing TX power even a little you save a significant amount of battery power
//This setting enables this gateway to work with remote nodes that have ATC enabled to
//dial their power down to only the required level (ATC_RSSI)
#define ENABLE_ATC    //comment out this line to disable AUTO TRANSMISSION CONTROL
#define ATC_RSSI      -80
//*********************************************************************************************

#ifdef __AVR_ATmega1284P__
#define LED           15 // Moteino MEGAs have LEDs on D15
#define FLASH_SS      23 // and FLASH SS on D23
#else
#define LED           9 // Moteinos have LEDs on D9
#define FLASH_SS      8 // and FLASH SS on D8
#endif

#define SERIAL_BAUD   115200

/// @dir roomNode
/// New version of the Room Node (derived from rooms.pde).
// 2010-10-19 <jc@wippler.nl> http://opensource.org/licenses/mit-license.php

// see http://jeelabs.org/2010/10/20/new-roomnode-code/
// and http://jeelabs.org/2010/10/21/reporting-motion/

// The complexity in the code below comes from the fact that newly detected PIR
// motion needs to be reported as soon as possible, but only once, while all the
// other sensor values are being collected and averaged in a more regular cycle.

//02.08.2013 Raino Kolk Simplified for DHT22
//#define RF69_COMPAT 0

#include "DHT.h"
//#include <avr/sleep.h>
#include <util/atomic.h>
#include "EmonLib.h"

#define SERIAL  1   // set to 1 to also report readings on the serial port
#define DEBUG   1   // set to 1 to display each loop() run and PIR trigger

#define DHT22_PORT  6   // defined if DHT22 is connected to a port
#define DHTTYPE DHT22

#define MEASURE_PERIOD  5 * 10 // how often to measure, in tenths of seconds
#define ACK_WAIT_PERIOD_MS   40  // how soon to retry if ACK didn't come in, ms
#define RETRY_LIMIT     2   // maximum number of times to retry
#define REPORT_EVERY    1   // report every N measurement cycles
#define SMOOTH          3   // smoothing factor used for running averages

// set the sync mode to 2 if the fuses are still the Arduino default
// mode 3 (full powerdown) can only be used with 258 CK startup fuses
//#define RADIO_SYNC_MODE 2

//#define freq RF12_868MHZ

// The scheduler makes it easy to perform various tasks at various times:

enum { MEASURE, REPORT, TASK_END };

static word schedbuf[TASK_END];
//Scheduler scheduler (schedbuf, TASK_END);

// Other variables used in various places in the code:

static byte reportCount;    // count up until next report, i.e. packet send
static byte myNodeID = NODEID;       // node ID used for this unit

// This defines the structure of the packets which get sent out by wireless:

struct {
  int humi;  // humidity: 0..100
  int temp; // temperature: -500..+500 (tenths)
  byte lobat;  // supply voltage dropped under 3.1V: 0..1
  int bat; // temperature: -500..+500 (tenths)
} payload;

EnergyMonitor monitor;
boolean requestACK = true;

// Conditional code, depending on which sensors are connected and how:

#if DHT22_PORT
DHT dht22(DHT22_PORT, DHTTYPE);
#endif

SPIFlash flash(FLASH_SS, 0xEF30); //EF30 for 4mbit  Windbond chip (W25X40CL)

#ifdef ENABLE_ATC
RFM69_ATC radio;
#else
RFM69 radio;
#endif

// has to be defined because we're using the watchdog for low-power waiting
//ISR(WDT_vect) {
//  Sleepy::watchdogEvent();
//}

void setup () {
  blink(9);
#if SERIAL || DEBUG
  Serial.begin(SERIAL_BAUD);
  Serial.print("\n[roomNode.");
  Serial.print(NODEID);
  Serial.println("]");
  serialFlush();
#endif

  radio.initialize(FREQUENCY, NODEID, NETWORKID);
#ifdef IS_RFM69HW
  radio.setHighPower(); //uncomment only for RFM69HW!
#endif
  radio.encrypt(ENCRYPTKEY);
  //radio.setFrequency(919000000); //set frequency to some custom frequency

  //Auto Transmission Control - dials down transmit power to save battery (-100 is the noise floor, -90 is still pretty good)
  //For indoor nodes that are pretty static and at pretty stable temperatures (like a MotionMote) -90dBm is quite safe
  //For more variable nodes that can expect to move or experience larger temp drifts a lower margin like -70 to -80 would probably be better
  //Always test your ATC mote in the edge cases in your own environment to ensure ATC will perform as you expect
#ifdef ENABLE_ATC
  radio.enableAutoPower(ATC_RSSI);
#endif

  char buff[50];
  sprintf(buff, "\nTransmitting at %d Mhz...", FREQUENCY == RF69_433MHZ ? 433 : FREQUENCY == RF69_868MHZ ? 868 : 915);
  #if SERIAL || DEBUG
  Serial.println(buff);
  #endif

  if (flash.initialize())
  {
    #if SERIAL || DEBUG
    Serial.print("SPI Flash Init OK ... UniqueID (MAC): ");
    #endif
    flash.readUniqueId();
    #if SERIAL || DEBUG
    for (byte i = 0; i < 8; i++)
    {
      Serial.print(flash.UNIQUEID[i], HEX);
      Serial.print(' ');
    }
    Serial.println();
    #endif
  }
  else {
    #if SERIAL || DEBUG
    Serial.println("SPI Flash MEM not found (is chip soldered?)...");
    #endif
  }

#ifdef ENABLE_ATC
  #if SERIAL || DEBUG
  Serial.println("RFM69_ATC Enabled (Auto Transmission Control)\n");
  #endif
#endif
//  Sleepy::loseSomeTime(1000); //for DHT warming
  reportCount = REPORT_EVERY;     // report right away for easy debugging
//  scheduler.timer(MEASURE, 0);    // start the measurement loop going
  //radio.sleep();
}

// utility code to perform simple smoothing as a running average
static int smoothedAverage(int prev, int next, byte firstTime = 0) {
  if (firstTime)
    return next;
  return ((SMOOTH - 1) * prev + next + SMOOTH / 2) / SMOOTH;
}

// spend a little time in power down mode while the DHT22 does a measurement
static void shtDelay() {
//  Sleepy::loseSomeTime(32); // must wait at least 20 ms
}

static long readVcc() {
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
#if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
  ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
#elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
  ADMUX = _BV(MUX5) | _BV(MUX0);
#elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
  ADMUX = _BV(MUX3) | _BV(MUX2);
#else
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
#endif

  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA, ADSC)); // measuring

  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH
  uint8_t high = ADCH; // unlocks both

  long result = (high << 8) | low;

  result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  return result; // Vcc in millivolts
}

// readout all the sensors and other values
static void doMeasure() {
  byte firstTime = payload.humi == 0; // special case to init running avg

  payload.lobat = 0;

#if DHT22_PORT
  int *result;
  shtDelay();
  result = dht22.readHumidityTemp();
#if SERIAL
  Serial.println();
  Serial.print(result[0]);
  Serial.println();
  Serial.print(result[1]);
  Serial.println();
  delay(2);
#endif
  float h = result[0];
  float t = result[1];

  int humi = h, temp = t;
#if SERIAL
  Serial.println();
  Serial.print(h);
  Serial.println();
  Serial.print(t);
  Serial.println();
  delay(2);
#endif

//  payload.humi = smoothedAverage(payload.humi, humi, firstTime);
//  payload.temp = smoothedAverage(payload.temp, temp, firstTime);
    payload.humi = humi;
    payload.temp = temp;
#endif
  payload.bat = readVcc();
}

static void serialFlush () {
#if ARDUINO >= 100 && SERIAL
  Serial.flush();
#endif
  delay(2); // make sure tx buf is empty before going back to sleep
}

// send packet and wait for ack when there is a motion trigger
static void doTrigger() {
#if SERIAL
  Serial.print("ROOM ");
  Serial.print((int) payload.humi);
  Serial.print(' ');
  Serial.print((int) payload.temp);
  Serial.println();
  serialFlush();
#endif
  Serial.println("Start sending");
  if (radio.sendWithRetry(GATEWAYID, (const void*)(&payload), sizeof(payload), RETRY_LIMIT, ACK_WAIT_PERIOD_MS)) {
    uint32_t starttime = millis();
    uint32_t endtime = starttime;
    while (!((endtime - starttime) > 200)) // do this loop for up to 1000mS
    {
      if (radio.receiveDone()) {
        if (radio.ACKRequested()) radio.sendACK();
      }
      endtime = millis();
    }
    radio.sleep();
    blink(LED);

    // reset scheduling to start a fresh measurement cycle
//    scheduler.timer(MEASURE, MEASURE_PERIOD);
    Serial.println("Data sent! Going to sleep!");
    return;
  }
//  scheduler.timer(MEASURE, MEASURE_PERIOD);
  radio.sleep();
#if DEBUG
  Serial.println("no ack! Data may be not sent! Going to sleep!");
  serialFlush();
#endif
}

void blink (byte pin) {
  pinMode(pin, OUTPUT);
  for (byte i = 0; i < 6; ++i) {
    delay(100);
    digitalWrite(pin, !digitalRead(pin));
  }
}

void sleepOneMinute()
{
  for (int i = 0; i < 8; i++) { 
     LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF); 
  }
}

void loop () {
#if DEBUG
  Serial.print('.');
  serialFlush();
#endif
doMeasure();
doTrigger();
serialFlush();
sleepOneMinute();

//Sleepy::loseSomeTime(5000);
//  switch (scheduler.pollWaiting()) {
//
//    case MEASURE:
//      // reschedule these measurements periodically
//      scheduler.timer(MEASURE, MEASURE_PERIOD);
//      // every so often, a report needs to be sent out
//      if (++reportCount >= REPORT_EVERY) {
//        reportCount = 0;
//        doMeasure();
//        scheduler.timer(REPORT, 0);
//      }
//      break;
//
//    case REPORT:
//      doTrigger();
//      //radio.sleep();
//      break;
//  }
}
