#include <RFM69.h>         //get it here: https://www.github.com/lowpowerlab/rfm69
#include <RFM69_ATC.h>     //get it here: https://www.github.com/lowpowerlab/rfm69

//*********************************************************************************************
//************ IMPORTANT SETTINGS - YOU MUST CHANGE/CONFIGURE TO FIT YOUR HARDWARE ************
//*********************************************************************************************
//11 magamistuba
//12 lastetuba
//13 meriel
//14 elutuba
#define NODEID        11    //must be unique for each node on same network (range up to 254, 255 is used for broadcast)
#define NETWORKID     210  //the same on all nodes that talk to each other (range up to 255)
#define GATEWAYID     1
//Match frequency to the hardware version of the radio on your Moteino (uncomment one):
//#define FREQUENCY   RF69_433MHZ
#define FREQUENCY   RF69_868MHZ
//#define FREQUENCY     RF69_915MHZ
#define ENCRYPTKEY    "sampleEncryptKey" //exactly the same 16 characters/bytes on all nodes!
//#define IS_RFM69HW    //uncomment only for RFM69HW! Leave out if you have RFM69W!
//*********************************************************************************************
//Auto Transmission Control - dials down transmit power to save battery
//Usually you do not need to always transmit at max output power
//By reducing TX power even a little you save a significant amount of battery power
//This setting enables this gateway to work with remote nodes that have ATC enabled to
//dial their power down to only the required level (ATC_RSSI)
//#define ENABLE_ATC    //comment out this line to disable AUTO TRANSMISSION CONTROL
#define ATC_RSSI      -80
//*********************************************************************************************

#ifdef ENABLE_ATC
RFM69_ATC radio(4, 0, true, 0);
#else
RFM69 radio(4, 0, true, 0);
#endif

struct {
  int humi;  // humidity: 0..100
  int temp; // temperature: -500..+500 (tenths)
  byte lobat;  // supply voltage dropped under 3.1V: 0..1
  int bat; // temperature: -500..+500 (tenths)
} payload;

void setup() {
  radio.initialize(FREQUENCY, NODEID, NETWORKID);
#ifdef IS_RFM69HW
  radio.setHighPower(); //uncomment only for RFM69HW!
#endif
  radio.encrypt(ENCRYPTKEY);
  //radio.setFrequency(919000000); //set frequency to some custom frequency

  //Auto Transmission Control - dials down transmit power to save battery (-100 is the noise floor, -90 is still pretty good)
  //For indoor nodes that are pretty static and at pretty stable temperatures (like a MotionMote) -90dBm is quite safe
  //For more variable nodes that can expect to move or experience larger temp drifts a lower margin like -70 to -80 would probably be better
  //Always test your ATC mote in the edge cases in your own environment to ensure ATC will perform as you expect
#ifdef ENABLE_ATC
  radio.enableAutoPower(ATC_RSSI);
#endif
}

void loop() {
  payload.temp = 20;
  if (radio.sendWithRetry(GATEWAYID, (const void*)(&payload), sizeof(payload))) {
      long starttime = millis();
      long endtime = starttime;
      while (endtime > starttime && (endtime - starttime) <= 1000) // do this loop for up to 1000mS
      {
        if (radio.receiveDone()) {
          if (radio.ACKRequested()) radio.sendACK();
        }
        endtime = millis();
      }
    }
    delay(5000);
}
