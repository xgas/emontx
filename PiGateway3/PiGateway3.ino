#define LED_PIN     9 

static void activityLed (byte on) {
#ifdef LED_PIN
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, on);
#endif
}

void setup() {
  Serial.begin(38400); 
}

void loop() {
  if(Serial.available()) {
    int r = Serial.read();
    Serial.write("--");
    Serial.write(r);
    Serial.write("--");
    if(r == '1') activityLed(1);
    else activityLed(0);
    if(r == 'A') activityLed(1);
    else activityLed(0);
  }
}
