// Example testing sketch for various DHT humidity/temperature sensors
// Written by ladyada, public domain

#include "DHT.h"
#define freq RF12_868MHZ   

#include "EmonLib.h"
EnergyMonitor monitor;

const int nodeID = 14;                                                  // emonTx RFM12B node ID
const int networkGroup = 210; 

const int UNO = 1;                                                      // Set to 0 if your not using the UNO bootloader (i.e using Duemilanove) - All Atmega's shipped from OpenEnergyMonitor come with Arduino Uno bootloader
#include <avr/wdt.h>                                                    // the UNO bootloader 

#include <JeeLib.h>                                                     // Download JeeLib: http://github.com/jcw/jeelib
ISR(WDT_vect) { Sleepy::watchdogEvent(); }
  
typedef struct { uint8_t temp1, temp2, humidity1, humidity2; int battery;} PayloadTX;
PayloadTX emontx;   // neat way of packaging data for RF comms

#define DHTPIN 6     // what pin we're connected to

// Uncomment whatever type you're using!
//#define DHTTYPE DHT11   // DHT 11 
#define DHTTYPE DHT22   // DHT 22  (AM2302)
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

DHT dht(DHTPIN, DHTTYPE);
const int LEDpin = 9; 
const int POWERpin = 7;  

const int sleepSeconds = 10; 
uint8_t *data;

void setup() {
  Serial.begin(9600); 
  Serial.println("DHTxx test!");
  
  pinMode(LEDpin, OUTPUT);                                              // Setup indicator LED
  digitalWrite(LEDpin, HIGH);
  
  pinMode(POWERpin, OUTPUT);                                              // Setup indicator LED
  digitalWrite(POWERpin, HIGH);
  
  rf12_initialize(nodeID, freq, networkGroup);                          // initialize RFM12B
  rf12_sleep(RF12_SLEEP);
  
  dht.begin();
  if (UNO) wdt_enable(WDTO_8S);  
}

void loop() {
  delay(1000);
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  data = dht.readData();
  
  emontx.battery = monitor.readVcc();

  // check if returns are valid, if they are NaN (not a number) then something went wrong!
  if (isnan(t) || isnan(h)) {
    Serial.println("Failed to read from DHT");
  } else {
    emontx.temp1 = data[0];
    emontx.temp2 = data[1];
    emontx.humidity1 = data[2];
    emontx.humidity2 = data[3];
    
    Serial.print(emontx.temp1);
    Serial.print(" ");
    Serial.println(emontx.temp2);
    delay(100);
    send_rf_data();
    
    Serial.print("Humidity: "); 
    Serial.print(h);
    Serial.print(" %\t");
    Serial.print("Temperature: "); 
    Serial.print(t);
    Serial.println(" *C");
  }
  digitalWrite(LEDpin, HIGH);
  delay(100);
  digitalWrite(LEDpin, LOW);
  
  digitalWrite(POWERpin, LOW);  
  sleep(sleepSeconds);
  Serial.println("wakeUp");
  digitalWrite(POWERpin, HIGH);
  delay(100);
}

void sleep(int secondsToSleep) {
  Serial.print("Go to sleep for "); 
  Serial.println(secondsToSleep);
  delay(100);
  Sleepy::loseSomeTime(secondsToSleep * 1000);  
}
