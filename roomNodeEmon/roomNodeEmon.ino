/// @dir roomNode
/// New version of the Room Node (derived from rooms.pde).
// 2010-10-19 <jc@wippler.nl> http://opensource.org/licenses/mit-license.php

// see http://jeelabs.org/2010/10/20/new-roomnode-code/
// and http://jeelabs.org/2010/10/21/reporting-motion/

// The complexity in the code below comes from the fact that newly detected PIR
// motion needs to be reported as soon as possible, but only once, while all the
// other sensor values are being collected and averaged in a more regular cycle.

//02.08.2013 Raino Kolk Simplified for DHT22

#include <JeeLib.h>
#include "DHT.h"
#include <avr/sleep.h>
#include <util/atomic.h>
#include "EmonLib.h"

#define SERIAL  1   // set to 1 to also report readings on the serial port
#define DEBUG   1   // set to 1 to display each loop() run and PIR trigger

#define DHT22_PORT  6   // defined if DHT22 is connected to a port
#define DHTTYPE DHT22 

#define MEASURE_PERIOD  600 // how often to measure, in tenths of seconds
#define RETRY_PERIOD    10  // how soon to retry if ACK didn't come in
#define RETRY_LIMIT     5   // maximum number of times to retry
#define ACK_TIME        10  // number of milliseconds to wait for an ack
#define REPORT_EVERY    1   // report every N measurement cycles
#define SMOOTH          3   // smoothing factor used for running averages

// set the sync mode to 2 if the fuses are still the Arduino default
// mode 3 (full powerdown) can only be used with 258 CK startup fuses
#define RADIO_SYNC_MODE 2

#define freq RF12_868MHZ 

// The scheduler makes it easy to perform various tasks at various times:

enum { MEASURE, REPORT, TASK_END };

static word schedbuf[TASK_END];
Scheduler scheduler (schedbuf, TASK_END);

// Other variables used in various places in the code:

const byte nodeID = 14;                                                  // emonTx RFM12B node ID
const int networkGroup = 210;

const int LEDpin = 9; 
const int POWERpin = 7;  

static byte reportCount;    // count up until next report, i.e. packet send
static byte myNodeID = nodeID;       // node ID used for this unit

// This defines the structure of the packets which get sent out by wireless:

struct {
    int humi  :16;  // humidity: 0..100
    int temp   :16; // temperature: -500..+500 (tenths)
    byte lobat :8;  // supply voltage dropped under 3.1V: 0..1
    int bat    :16; // temperature: -500..+500 (tenths)
} payload;

EnergyMonitor monitor;

// Conditional code, depending on which sensors are connected and how:

#if DHT22_PORT
    DHT dht22(DHT22_PORT, DHTTYPE);
#endif

// has to be defined because we're using the watchdog for low-power waiting
ISR(WDT_vect) { Sleepy::watchdogEvent(); }

// utility code to perform simple smoothing as a running average
static int smoothedAverage(int prev, int next, byte firstTime =0) {
    if (firstTime)
        return next;
    return ((SMOOTH - 1) * prev + next + SMOOTH / 2) / SMOOTH;
}

// spend a little time in power down mode while the DHT22 does a measurement
static void shtDelay () {
    Sleepy::loseSomeTime(32); // must wait at least 20 ms
}

// wait a few milliseconds for proper ACK to me, return true if indeed received
static byte waitForAck() {
    MilliTimer ackTimer;
    while (!ackTimer.poll(ACK_TIME)) {
        if (rf12_recvDone() && rf12_crc == 0 &&
                // see http://talk.jeelabs.net/topic/811#post-4712
                rf12_hdr == (RF12_HDR_DST | RF12_HDR_CTL | myNodeID))
            return 1;
        set_sleep_mode(SLEEP_MODE_IDLE);
        sleep_mode();
    }
    return 0;
}

// readout all the sensors and other values
static void doMeasure() {   
    byte firstTime = payload.humi == 0; // special case to init running avg
    
    payload.lobat = rf12_lowbat();

    #if DHT22_PORT
        int *result;
        result = dht22.readHumidityTemp();
        #if SERIAL
          Serial.println();
          Serial.print(result[0]);
          Serial.println();
          Serial.print(result[1]);
          Serial.println();
          delay(2);
        #endif
        
        int humi = result[0];        
        int temp = result[1];

        #if SERIAL
          Serial.println();
          Serial.print(humi);
          Serial.println();
          Serial.print(temp);
          Serial.println();
          delay(2);
        #endif

        payload.humi = smoothedAverage(payload.humi, humi, firstTime);
        payload.temp = smoothedAverage(payload.temp, temp, firstTime);
    #endif
    payload.bat = monitor.readVcc();
}

static void serialFlush () {
    #if ARDUINO >= 100
        Serial.flush();
    #endif  
    delay(2); // make sure tx buf is empty before going back to sleep
}

// periodic report, i.e. send out a packet and optionally report on serial port
static void doReport() {
    rf12_sleep(RF12_WAKEUP);
    rf12_sendNow(RF12_HDR_ACK, &payload, sizeof payload);
    rf12_sendWait(RADIO_SYNC_MODE);
    rf12_sleep(RF12_SLEEP);

    #if SERIAL
        Serial.print("ROOM ");
        Serial.print((int) payload.humi);
        Serial.print(' ');
        Serial.print((int) payload.temp);
        Serial.println();
        serialFlush();
    #endif
}

// send packet and wait for ack when there is a motion trigger
static void doTrigger() {
  #if SERIAL
        Serial.print("ROOM ");
        Serial.print((int) payload.humi);
        Serial.print(' ');
        Serial.print((int) payload.temp);
        Serial.println();
        serialFlush();
    #endif

    for (byte i = 0; i < RETRY_LIMIT; ++i) {
        rf12_sleep(RF12_WAKEUP);
        rf12_sendNow(RF12_HDR_ACK, &payload, sizeof payload);
        rf12_sendWait(RADIO_SYNC_MODE);
        byte acked = waitForAck();
        rf12_sleep(RF12_SLEEP);
        
        digitalWrite(LEDpin, HIGH);
        delay(10);
        digitalWrite(LEDpin, LOW);

        if (acked) {
            #if DEBUG
                Serial.print(" ack ");
                Serial.println((int) i);
                serialFlush();
            #endif
            // reset scheduling to start a fresh measurement cycle
            scheduler.timer(MEASURE, MEASURE_PERIOD);
            return;
        }
        
        delay(RETRY_PERIOD * 100);
    }
    scheduler.timer(MEASURE, MEASURE_PERIOD);
    #if DEBUG
        Serial.println(" no ack!");
        serialFlush();
    #endif
}

void blink (byte pin) {
    for (byte i = 0; i < 6; ++i) {
        delay(100);
        digitalWrite(pin, !digitalRead(pin));
    }
}

void setup () {
  pinMode(LEDpin, OUTPUT);                                              // Setup indicator LED
  digitalWrite(LEDpin, HIGH);
  
  pinMode(POWERpin, OUTPUT);                                              // Setup indicator LED
  digitalWrite(POWERpin, HIGH);
  #if SERIAL || DEBUG
    Serial.begin(57600);
    Serial.print("\n[roomNode.3]");
    rf12_initialize(nodeID, freq, networkGroup); 
    //myNodeID = rf12_config();
    serialFlush();
  #else
    rf12_initialize(nodeID, freq, networkGroup);
    //myNodeID = rf12_config(0); // don't report info on the serial port
  #endif
    
  rf12_sleep(RF12_SLEEP); // power down

  reportCount = REPORT_EVERY;     // report right away for easy debugging
  scheduler.timer(MEASURE, 0);    // start the measurement loop going
}

void loop () {
    #if DEBUG
        Serial.print('.');
        serialFlush();
    #endif
    
    switch (scheduler.pollWaiting()) {

        case MEASURE:
            // reschedule these measurements periodically
            scheduler.timer(MEASURE, MEASURE_PERIOD);
    
            doMeasure();

            // every so often, a report needs to be sent out
            if (++reportCount >= REPORT_EVERY) {
                reportCount = 0;
                scheduler.timer(REPORT, 0);
            }
            break;
            
        case REPORT:
            doTrigger();
            break;
    }
}
