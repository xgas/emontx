#include <Ports.h>
#include <PortsBMP085.h>
#include <PortsLCD.h>
#include <PortsSHT11.h>
#include <SoftwareSerial.h>

#include <RFM69.h>         //get it here: https://www.github.com/lowpowerlab/rfm69
#include <RFM69_ATC.h>     //get it here: https://www.github.com/lowpowerlab/rfm69
#include <SPIFlash.h>      //get it here: https://www.github.com/lowpowerlab/spiflash
#include <SPI.h>           //included with Arduino IDE install (www.arduino.cc)

//*********************************************************************************************
//************ IMPORTANT SETTINGS - YOU MUST CHANGE/CONFIGURE TO FIT YOUR HARDWARE ************
//*********************************************************************************************
//12 lastetuba
#define NODEID        20    //must be unique for each node on same network (range up to 254, 255 is used for broadcast)
#define NETWORKID     210  //the same on all nodes that talk to each other (range up to 255)
#define GATEWAYID     1
//Match frequency to the hardware version of the radio on your Moteino (uncomment one):
//#define FREQUENCY   RF69_433MHZ
#define FREQUENCY   RF69_868MHZ
//#define FREQUENCY     RF69_915MHZ
#define ENCRYPTKEY    "sampleEncryptKey" //exactly the same 16 characters/bytes on all nodes!
//#define IS_RFM69HW    //uncomment only for RFM69HW! Leave out if you have RFM69W!
//*********************************************************************************************
//Auto Transmission Control - dials down transmit power to save battery
//Usually you do not need to always transmit at max output power
//By reducing TX power even a little you save a significant amount of battery power
//This setting enables this gateway to work with remote nodes that have ATC enabled to
//dial their power down to only the required level (ATC_RSSI)
#define ENABLE_ATC    //comment out this line to disable AUTO TRANSMISSION CONTROL
#define ATC_RSSI      -80
//*********************************************************************************************

#ifdef __AVR_ATmega1284P__
#define LED           15 // Moteino MEGAs have LEDs on D15
#define FLASH_SS      23 // and FLASH SS on D23
#else
#define LED           9 // Moteinos have LEDs on D9
#define FLASH_SS      8 // and FLASH SS on D8
#endif

#define SERIAL_BAUD   9600

/// @dir roomNode
/// New version of the Room Node (derived from rooms.pde).
// 2010-10-19 <jc@wippler.nl> http://opensource.org/licenses/mit-license.php

// see http://jeelabs.org/2010/10/20/new-roomnode-code/
// and http://jeelabs.org/2010/10/21/reporting-motion/

// The complexity in the code below comes from the fact that newly detected PIR
// motion needs to be reported as soon as possible, but only once, while all the
// other sensor values are being collected and averaged in a more regular cycle.

#include <avr/sleep.h>
#include <util/atomic.h>
#include "EmonLib.h"

#define SERIAL  1   // set to 1 to also report readings on the serial port
#define DEBUG   1   // set to 1 to display each loop() run and PIR trigger

#define DRIVE_PIN 7
#define READ_PIN 17

#define MEASURE_PERIOD  10 * 10 // how often to measure, in tenths of seconds
#define RETRY_PERIOD    1 *10  // how soon to retry if ACK didn't come in
#define RETRY_LIMIT     10   // maximum number of times to retry
#define REPORT_EVERY    1   // report every N measurement cycles
#define SMOOTH          3   // smoothing factor used for running averages

SoftwareSerial mySerial(READ_PIN, 5); // RX, TX

// set the sync mode to 2 if the fuses are still the Arduino default
// mode 3 (full powerdown) can only be used with 258 CK startup fuses
//#define RADIO_SYNC_MODE 2

//#define freq RF12_868MHZ

// The scheduler makes it easy to perform various tasks at various times:

enum { MEASURE, REPORT, TASK_END };

static word schedbuf[TASK_END];
Scheduler scheduler (schedbuf, TASK_END);

// Other variables used in various places in the code:

static byte reportCount;    // count up until next report, i.e. packet send
static byte myNodeID = NODEID;       // node ID used for this unit

// This defines the structure of the packets which get sent out by wireless:

struct {
  int distance;  // humidity: 0..100
  byte lobat;  // supply voltage dropped under 3.1V: 0..1
  int bat; // temperature: -500..+500 (tenths)
} payload;

EnergyMonitor monitor;
boolean requestACK = true;

// Conditional code, depending on which sensors are connected and how:
SPIFlash flash(FLASH_SS, 0xEF30); //EF30 for 4mbit  Windbond chip (W25X40CL)

#ifdef ENABLE_ATC
RFM69_ATC radio;
#else
RFM69 radio;
#endif

const byte numChars = 32;
char receivedChars[numChars];
boolean newData = false;

// has to be defined because we're using the watchdog for low-power waiting
ISR(WDT_vect) {
  Sleepy::watchdogEvent();
}

void setup () {
  mySerial.begin(9600);
#if SERIAL || DEBUG
  Serial.begin(SERIAL_BAUD);
  Serial.print("\n[roomNode.");
  Serial.print(NODEID);
  Serial.println("]");
  serialFlush();
#endif

  radio.initialize(FREQUENCY, NODEID, NETWORKID);
#ifdef IS_RFM69HW
  radio.setHighPower(); //uncomment only for RFM69HW!
#endif
  radio.encrypt(ENCRYPTKEY);
  //radio.setFrequency(919000000); //set frequency to some custom frequency

  //Auto Transmission Control - dials down transmit power to save battery (-100 is the noise floor, -90 is still pretty good)
  //For indoor nodes that are pretty static and at pretty stable temperatures (like a MotionMote) -90dBm is quite safe
  //For more variable nodes that can expect to move or experience larger temp drifts a lower margin like -70 to -80 would probably be better
  //Always test your ATC mote in the edge cases in your own environment to ensure ATC will perform as you expect
#ifdef ENABLE_ATC
  radio.enableAutoPower(ATC_RSSI);
#endif

  char buff[50];
  sprintf(buff, "\nTransmitting at %d Mhz...", FREQUENCY == RF69_433MHZ ? 433 : FREQUENCY == RF69_868MHZ ? 868 : 915);
  Serial.println(buff);

  if (flash.initialize())
  {
    Serial.print("SPI Flash Init OK ... UniqueID (MAC): ");
    flash.readUniqueId();
    for (byte i = 0; i < 8; i++)
    {
      Serial.print(flash.UNIQUEID[i], HEX);
      Serial.print(' ');
    }
    Serial.println();
  }
  else
    Serial.println("SPI Flash MEM not found (is chip soldered?)...");

#ifdef ENABLE_ATC
  Serial.println("RFM69_ATC Enabled (Auto Transmission Control)\n");
#endif

  pinMode(READ_PIN, INPUT);
  pinMode(DRIVE_PIN, OUTPUT);
  digitalWrite(DRIVE_PIN, LOW);

  scheduler.timer(MEASURE, 0);    // start the measurement loop going
}

void recvWithEndMarker() {
  newData = false;
  static byte ndx = 0;
  char endMarker = 13;
  char rc;

  // if (Serial.available() > 0) {
  while (mySerial.available() > 0) {
    rc = mySerial.read();
    if (rc == 'R') {
      ndx = 0;
    }

    if (rc != endMarker) {
      receivedChars[ndx] = rc;
      ndx++;
      if (ndx >= numChars) {
        ndx = numChars - 1;
      }
    }
    else {
      receivedChars[ndx] = '\0'; // terminate the string
      ndx = 0;
      newData = true;
      return;
    }
  }
}

// utility code to perform simple smoothing as a running average
static int smoothedAverage(int prev, int next, byte firstTime = 0) {
  if (firstTime)
    return next;
  return ((SMOOTH - 1) * prev + next + SMOOTH / 2) / SMOOTH;
}

// spend a little time in power down mode while the DHT22 does a measurement
static void shtDelay() {
  Sleepy::loseSomeTime(32); // must wait at least 20 ms
}

static long readVcc() {
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
#if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
  ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
#elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
  ADMUX = _BV(MUX5) | _BV(MUX0);
#elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
  ADMUX = _BV(MUX3) | _BV(MUX2);
#else
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
#endif

  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA, ADSC)); // measuring

  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH
  uint8_t high = ADCH; // unlocks both

  long result = (high << 8) | low;

  result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  return result; // Vcc in millivolts
}

float measure() {
  digitalWrite(DRIVE_PIN, HIGH);
  delay(500);
  digitalWrite(DRIVE_PIN, LOW);
  unsigned int measureVal = 0;
  newData = true;
  while(newData) {
    recvWithEndMarker();
    if(newData) {
      measureVal = String(receivedChars).substring(1).toInt();
    }
    Serial.println(receivedChars);
  }

  unsigned long first = 0;
  float elapsed = 0;
  byte up = 0;

  //measureVal = pulseIn(READ_PIN, HIGH);
  digitalWrite(DRIVE_PIN, LOW);
  //float inches = measureVal / 147;
  //change inches to centimetres
  //float cm = inches * 2.54;
  float cm = measureVal / 10;
  //Serial.print(inches);
  //Serial.print("in, ");
  Serial.print(cm);
  Serial.print("cm, ");
  Serial.print(measureVal);
  Serial.print("mm");
  Serial.println();
  return cm;
}

// readout all the sensors and other values
static void doMeasure() {
  payload.lobat = 0;//rf12_lowbat();
  payload.distance = measure();
  payload.bat = readVcc();
}

static void serialFlush () {
#if ARDUINO >= 100
  Serial.flush();
#endif
  delay(2); // make sure tx buf is empty before going back to sleep
}

// send packet and wait for ack when there is a motion trigger
static void doTrigger() {
#if SERIAL
  Serial.print("SONAR ");
  Serial.print((int) payload.distance);
  Serial.println();
  serialFlush();
#endif

  for (byte i = 0; i < RETRY_LIMIT; ++i) {
    if (radio.sendWithRetry(GATEWAYID, (const void*)(&payload), sizeof(payload))) {
      long starttime = millis();
      long endtime = starttime;
      while (!((endtime - starttime) > 1000)) // do this loop for up to 1000mS
      {
        if (radio.receiveDone()) {
          if (radio.ACKRequested()) radio.sendACK();
        }
        endtime = millis();
      }

      // reset scheduling to start a fresh measurement cycle
      scheduler.timer(MEASURE, MEASURE_PERIOD);
      return;
    }
    delay(RETRY_PERIOD * 100);
  }
  scheduler.timer(MEASURE, MEASURE_PERIOD);
#if DEBUG
  Serial.println(" no ack!");
  serialFlush();
#endif
}

void blink (byte pin) {
  for (byte i = 0; i < 6; ++i) {
    delay(100);
    digitalWrite(pin, !digitalRead(pin));
  }
}

void loop () {
#if DEBUG
  Serial.print('.');
  serialFlush();
#endif

  switch (scheduler.pollWaiting()) {

    case MEASURE:
      // reschedule these measurements periodically
      scheduler.timer(MEASURE, MEASURE_PERIOD);

      doMeasure();
      radio.sleep();

      // every so often, a report needs to be sent out
      if (++reportCount >= REPORT_EVERY) {
        reportCount = 0;
        scheduler.timer(REPORT, 0);
      }
      break;

    case REPORT:
      doTrigger();
      break;
  }
}
